package com.taplifetechnology.androidlibrary

import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.Transformation

fun View.marginStart(margin: Int)
{
    if (this.layoutParams is ViewGroup.MarginLayoutParams)
    {
        val p = this.layoutParams as ViewGroup.MarginLayoutParams
        p.setMargins(margin, p.topMargin, p.marginEnd, p.bottomMargin)
        this.requestLayout()
    }
}

fun View. marginEnd(margin: Int)
{
    if (this.layoutParams is ViewGroup.MarginLayoutParams)
    {
        val p = this.layoutParams as ViewGroup.MarginLayoutParams
        p.setMargins(p.marginStart, p.topMargin, margin, p.bottomMargin)
        this.requestLayout()
    }
}

fun View.marginTop(margin: Int)
{
    if (this.layoutParams is ViewGroup.MarginLayoutParams)
    {
        val p = this.layoutParams as ViewGroup.MarginLayoutParams
        p.setMargins(p.marginStart, margin, p.marginEnd, p.bottomMargin)
        this.requestLayout()
    }
}

fun View.marginBottom(margin: Int)
{
    if (this.layoutParams is ViewGroup.MarginLayoutParams)
    {
        val p = this.layoutParams as ViewGroup.MarginLayoutParams
        p.setMargins(p.marginStart, p.topMargin, p.marginEnd, margin)
        this.requestLayout()
    }
}

fun View.expand(targetHeight: Int)
{
    val v = this
    v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    v.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
    // Older versions of android (pre API 21) cancel animations for views with a height of 0.
    //v.layoutParams.height = 1
    v.visibility = View.VISIBLE
    val a = object : Animation()
    {
        override fun applyTransformation(interpolatedTime: Float, t: Transformation)
        {
            v.layoutParams.height =
                if (interpolatedTime == 1f)
                    ViewGroup.LayoutParams.WRAP_CONTENT
                else
                    (targetHeight * interpolatedTime).toInt()
            v.requestLayout()
            log("", " interpolate time $interpolatedTime  ${v.layoutParams.height}")
        }

        override fun willChangeBounds(): Boolean
        {
            return true
        }

    }

    // 1dp/ms
    a.duration = (targetHeight / v.context.resources.displayMetrics.density).toLong()
    v.startAnimation(a)
}

fun View.collapse()
{
    val v = this
    val initialHeight = v.measuredHeight

    val a = object : Animation()
    {
        override fun applyTransformation(interpolatedTime: Float, t: Transformation)
        {
            if (interpolatedTime == 1f)
            {
                v.visibility = View.GONE
            }
            else
            {
                v.layoutParams.height = initialHeight - (initialHeight * interpolatedTime).toInt()
                v.requestLayout()
            }
        }

        override fun willChangeBounds(): Boolean
        {
            return true
        }
    }

    // 1dp/ms
    a.duration = (initialHeight / v.context.resources.displayMetrics.density).toLong()
    v.startAnimation(a)
}
