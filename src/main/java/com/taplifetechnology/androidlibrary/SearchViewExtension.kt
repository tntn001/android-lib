package com.taplifetechnology.androidlibrary

import android.app.Activity
import android.os.CountDownTimer
import androidx.appcompat.widget.SearchView


fun SearchView.delaySearch(activity: Activity, millisecond: Long, result: (String) -> Unit)
{
    this.setOnQueryTextListener(object : SearchView.OnQueryTextListener
    {
        private var mIsDelayGetData = false
        private var mTimerTask: CountDownTimer? = null

        private var mTimeRemainingCountdown = 0L
        override fun onQueryTextSubmit(query: String?): Boolean
        {
            return false
        }

        override fun onQueryTextChange(newText: String?): Boolean
        {
            if (mIsDelayGetData)
            {
                mTimerTask?.cancel()
                mTimerTask = object : CountDownTimer(mTimeRemainingCountdown, 10)
                {
                    override fun onFinish()
                    {

                        activity.runOnUiThread()
                        {
                            mIsDelayGetData = true
                            result.invoke(newText ?: "")
                            mTimerTask = object : CountDownTimer(millisecond, 10)
                            {
                                override fun onFinish()
                                {
                                    mIsDelayGetData = false
                                }

                                override fun onTick(p0: Long)
                                {
                                    mTimeRemainingCountdown = p0
                                }

                            }
                            mTimerTask!!.start()
                        }
                    }

                    override fun onTick(p0: Long)
                    {
                        mTimeRemainingCountdown = p0
                    }

                }
                mTimerTask!!.start()
            }
            else
            {
                mIsDelayGetData = true
                result.invoke(newText ?: "")
                mTimerTask = object : CountDownTimer(millisecond, 10)
                {
                    override fun onFinish()
                    {
                        mIsDelayGetData = false
                    }

                    override fun onTick(p0: Long)
                    {
                        mTimeRemainingCountdown = p0
                    }
                }
                mTimerTask!!.start()
            }
            return true
        }

    })
}