@file:Suppress("UNREACHABLE_CODE")

package com.taplifetechnology.androidlibrary

import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.net.Uri
import android.provider.MediaStore
import android.util.Log
import android.util.TypedValue
import android.view.View
import java.util.regex.Pattern
import kotlin.random.Random

fun DPtoPX(dp: Float): Int
{
    return (dp * Resources.getSystem().displayMetrics.density).toInt()
}

fun SPtoPX(sp: Float, context: Context): Float
{
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP,
            sp,
            context.resources.displayMetrics)
}

fun PXtoDP(px: Float): Number
{
    return (px / Resources.getSystem().displayMetrics.density)
}

fun log(tag: String = "", msg: String)
{
    val stackTrace: StackTraceElement = Exception().stackTrace.get(1)
    val fileName: String = stackTrace.fileName

    val info: String = stackTrace.methodName + " (" + fileName + ":" + stackTrace.lineNumber + ")"
    Log.e(info, "$tag: $msg")
}

fun getRealPathFromURI(context: Context, contentURI: Uri): String
{
    var result = ""

    val cursor = context.contentResolver.query(contentURI, null, null, null, null)

    if(cursor == null)
    {
        result = contentURI.path
    }
    else
    {
        if(cursor.moveToFirst())
        {
            val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            result = cursor.getString(idx)
        }

    }
    cursor?.close()
    return result
}

fun isValidEmailId(email: String): Boolean
{

    return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
            + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
            + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
            + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
            + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
            + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches()
}

fun View.initBackground(bgConfig: BackgroundConfig,
                        orientation: GradientDrawable.Orientation = GradientDrawable.Orientation.LEFT_RIGHT): Drawable
{
    val shape = GradientDrawable()
    shape.shape = GradientDrawable.RECTANGLE
    shape.cornerRadii = FloatArray(8)
    {
        when(it)
        {
            0, 1 -> bgConfig.topLeft.toFloat()
            2, 3 -> bgConfig.topRight.toFloat()
            4, 5 -> bgConfig.botRight.toFloat()
            6, 7 -> bgConfig.botLeft.toFloat()
            else -> 0f
        }
    }
    if(bgConfig.bgColorEnd != 0)
    {
        shape.orientation = orientation
        shape.colors = IntArray(2)
        {
            when(it)
            {
                0 -> bgConfig.bgColor
                else -> bgConfig.bgColorEnd
            }
        }
    }
    else
    {
        shape.setColor(bgConfig.bgColor)
    }

    shape.setStroke(bgConfig.strokeWidth, Color.TRANSPARENT)

    background = shape
    return shape
}

fun drawableToBitmap(drawable: Drawable?): Bitmap?
{
    drawable?.let()
    {
        if(drawable is BitmapDrawable)
        {
            return drawable.bitmap
        }

        var width = drawable.intrinsicWidth
        width = if(width > 0) width else 1
        var height = drawable.intrinsicHeight
        height = if(height > 0) height else 1

        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)
        return bitmap
    }
    return null
}

data class BackgroundConfig
(
    var topLeft: Int = 0,
    var topRight: Int = 0,
    var botLeft: Int = 0,
    var botRight: Int = 0,
    var bgColor: Int = 0,
    var bgColorEnd: Int = 0,
    var strokeWidth: Int = 0,
    var strokeColor: Int = 0,
    var strokeColorEnd: Int = 0,
    var isDash:Boolean = false
)

fun randomString(minLength: Int = 1, maxLength: Int = 16): String
{
    val chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
    val length = Random.nextInt(minLength, maxLength + 1)
    val rand = java.util.Random()

    val ret = StringBuilder()
    for(i in 0..length)
    {
        ret.append(chars[rand.nextInt(chars.length)])
    }
    return ret.toString()
}