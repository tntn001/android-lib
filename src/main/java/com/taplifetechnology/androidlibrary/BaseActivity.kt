package com.taplifetechnology.androidlibrary

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager.GET_META_DATA
import android.content.res.Resources
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.view.MenuItem
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import java.lang.Exception


abstract class BaseActivity: AppCompatActivity()
{
    protected open var mIsPreventCloseKeyboard = false

    companion object
    {
        var onCreatedActivity = {}
        var CurrentActivity: BaseActivity? = null
        private val mListResultListener = ArrayList<ActivityResultListener>()

        fun addOnResultListener(listener: ActivityResultListener)
        {
            mListResultListener.add(listener)
        }

        fun removeResultListener(listener: ActivityResultListener)
        {
            if(mListResultListener.contains(listener))
            {
                mListResultListener.remove(listener)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?)
    {
        super.onCreate(savedInstanceState, persistentState)
        changeLanguage()
    }

    private fun changeLanguage()
    {
        try
        {
            val info = packageManager.getActivityInfo(componentName, GET_META_DATA)
            if(info.labelRes != 0)
            {
                setTitle(info.labelRes)
            }
        }
        catch(e: Exception)
        {
            e.printStackTrace()
        }
    }

    override fun attachBaseContext(newBase: Context?)
    {
        if(newBase != null)
        {
            super.attachBaseContext(LocaleManager.setLocale(newBase))
        }
        else
        {
            super.attachBaseContext(newBase)
        }
    }

    fun getStringByKey(key: String): String
    {
        return try
        {
            resources.getString(resources.getIdentifier(key, "string", packageName))
        }
        catch(e: Resources.NotFoundException)
        {
            "Undefined key $key"
        }
    }

    override fun onResume()
    {
        CurrentActivity = this
        super.onResume()
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        CurrentActivity = this
    }

    override fun onAttachedToWindow()
    {
        super.onAttachedToWindow()
        onCreatedActivity.invoke()
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean
    {
        if(!mIsPreventCloseKeyboard)
        {
            val view = currentFocus
            if(view != null && (ev?.action == MotionEvent.ACTION_UP || ev?.action == MotionEvent.ACTION_MOVE
                        && (view is EditText) && !view::class.java.name.startsWith("android.webkit.")))
            {
                val scrcoord = IntArray(2)
                view.getLocationOnScreen(scrcoord)
                val x = ev.rawX + view.left - scrcoord[0]
                val y = ev.rawY + view.top - scrcoord[1]
                if(x < view.left || x > view.right || y < view.top || y > view.bottom)
                {
                    view.clearFocus()
                    (this@BaseActivity.getSystemService(Context.INPUT_METHOD_SERVICE) as (InputMethodManager)).hideSoftInputFromWindow(
                            (this.window.decorView.applicationWindowToken),
                            0)
                }

            }
        }
        return super.dispatchTouchEvent(ev)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean
    {
        when(item!!.itemId)
        {

            android.R.id.home ->
            {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)
    {
        super.onActivityResult(requestCode, resultCode, data)
        val clone = ArrayList(mListResultListener)
        for(i in 0 until clone.count())
        {
            clone[i].onResult(requestCode, resultCode, data)
        }
    }
}

interface ActivityResultListener
{
    fun onResult(requestCode: Int, resultCode: Int, data: Intent?)
}

fun getCurrentActivity(): BaseActivity
{
    return BaseActivity.CurrentActivity!!
}