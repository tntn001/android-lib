package com.taplifetechnology.androidlibrary

import java.lang.StringBuilder
import java.text.DecimalFormatSymbols
import java.time.format.DateTimeFormatter

/**
 * Check char sequence is number
 */
fun CharSequence?.isNumber(): Boolean
{
    val temp = StringBuilder(this)
    if(temp.toString().toIntOrNull()!= null)
    {
        return true
    }

    if(temp.toString().toFloatOrNull()!= null)
    {
        return true
    }
    if(temp.toString().toDoubleOrNull()!= null)
    {
        return true
    }
    if(temp.toString().toLongOrNull()!= null)
    {
        return true
    }

    this?.let()
    {
        val decimalSeparator = '.'
                //DecimalFormatSymbols.getInstance().decimalSeparator
        var isHaveSeparator = false
        for (i in 0 until this.length)
        {
            val item = this[i]
            if (item < '0' || item > '9')
            {
                if (item == decimalSeparator
                    && i != 0)
                {
                    if (isHaveSeparator)
                    {
                        return false
                    }
                    else
                    {
                        isHaveSeparator = true
                    }
                }
                else
                {
                    return false
                }
            }
        }
        return true
    } ?: return false
}