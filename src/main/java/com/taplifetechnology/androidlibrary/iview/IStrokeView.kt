package com.taplifetechnology.androidlibrary.iview

interface IStrokeView
{
    var strokeWidth: Int
    var strokeColor: Int
    var strokeColorEnd:Int
}