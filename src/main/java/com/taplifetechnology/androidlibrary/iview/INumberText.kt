package com.taplifetechnology.androidlibrary.iview

interface INumberText
{
    var isThousandsSeparate: Boolean
    var minNumberInLeftDecimal: Int
    var roundWithNumberInRightDecimal: Int
    var cutWithNumberInRightDecimal: Int
}