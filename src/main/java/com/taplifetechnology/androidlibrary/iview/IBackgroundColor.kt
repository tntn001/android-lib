package com.taplifetechnology.androidlibrary.iview

interface IBackgroundColor
{
    var bgColor: Int
    var bgColorEnd:Int
}