package com.taplifetechnology.androidlibrary.iview

interface ICornerView
{
    var cornerTopLeft: Int
    var cornerTopRight: Int
    var cornerBotLeft: Int
    var cornerBotRight: Int
}