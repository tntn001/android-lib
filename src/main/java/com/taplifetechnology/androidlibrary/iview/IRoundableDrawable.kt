package com.taplifetechnology.androidlibrary.iview

import android.graphics.drawable.Drawable

interface IRoundableDrawable
{
    var isRound:Boolean
    var drawable:Drawable
}