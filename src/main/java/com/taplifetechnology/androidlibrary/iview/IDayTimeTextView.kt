package com.taplifetechnology.androidlibrary.iview

import java.util.Date

interface IDayTimeTextView
{
    var dateInputFormat: String?
    var dateOutputFormat: String?
    var dateObject: Date?
}