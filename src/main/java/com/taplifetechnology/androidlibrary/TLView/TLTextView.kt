package com.taplifetechnology.androidlibrary.TLView

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.LinearGradient
import android.graphics.Paint
import android.graphics.Path
import android.graphics.RectF
import android.graphics.Shader
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import com.taplifetechnology.androidlibrary.BackgroundConfig
import com.taplifetechnology.androidlibrary.R
import com.taplifetechnology.androidlibrary.iview.IBackgroundColor
import com.taplifetechnology.androidlibrary.iview.ICornerView
import com.taplifetechnology.androidlibrary.iview.IDayTimeTextView
import com.taplifetechnology.androidlibrary.iview.INumberText
import com.taplifetechnology.androidlibrary.iview.IStrokeView
import com.taplifetechnology.androidlibrary.initBackground
import com.taplifetechnology.androidlibrary.isNumber
import com.taplifetechnology.androidlibrary.log
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class TLTextView : AppCompatTextView,
    IBackgroundColor,
    ICornerView,
    INumberText,
    IStrokeView,
    IDayTimeTextView
{
    override var bgColorEnd: Int = 0
    override var strokeColorEnd: Int = 0

    private val bgConfig = BackgroundConfig()

    private val path = Path()
    private val mPaint = Paint()
    private lateinit var mCornerRadius: FloatArray

    override var dateInputFormat: String? = ""
        set(value)
        {
            field = value
            invalidate()
            if (text.isNotEmpty()
                && text.isNotBlank()
                && !field.isNullOrEmpty()
                && !field.isNullOrBlank())
            {
                try
                {
                    dateObject = SimpleDateFormat(field, Locale.getDefault()).parse(text.toString())
                }
                catch (e: Exception)
                {
                    // e.printStackTrace()
                }
            }
        }
    override var dateOutputFormat: String? = ""
        set(value)
        {
            field = value
            if (text.isNotEmpty()
                && text.isNotBlank()
                && field.isNullOrBlank()
                && field.isNullOrEmpty())
            {
                if (!dateInputFormat.isNullOrBlank() && !dateInputFormat.isNullOrEmpty())
                {
                    try
                    {
                        m_isSetDateObject = true
                        text = SimpleDateFormat(dateOutputFormat, Locale.getDefault()).format(dateObject)
                    }
                    catch (e: Exception)
                    {
                        //e.printStackTrace()
                    }
                }
            }
        }
    override var dateObject: Date? = null
        set(value)
        {
            field = value
            if (!dateOutputFormat.isNullOrBlank()
                && !dateOutputFormat.isNullOrEmpty())
            {
                try
                {
                    m_isSetDateObject = true
                    text = SimpleDateFormat(dateOutputFormat, Locale.getDefault()).format(value)
                }
                catch (e: Exception)
                {
                    e.printStackTrace()
                }
            }
        }
    override var bgColor: Int = 0

    override var cornerTopLeft: Int = 0
        set(value)
        {
            field = value
            bgConfig.topLeft = value
            initBackground(bgConfig)
            path.reset()
            refreshCornerList()
            if (strokeWidth > 0)
            {
                if (strokeColorEnd != 0)
                {
                    mPaint.color = bgConfig.strokeColor
                    mPaint.shader = LinearGradient(0f, 0f, width.toFloat(), 0f, bgConfig.strokeColor, bgConfig.strokeColorEnd, Shader.TileMode.CLAMP)
                }
                else
                {
                    mPaint.shader = null
                    mPaint.color = bgConfig.strokeColor
                }
            }
            invalidate()
        }
    override var cornerTopRight: Int = 0
        set(value)
        {
            field = value
            bgConfig.topRight = value
            initBackground(bgConfig)
            path.reset()
            refreshCornerList()
            if (strokeWidth > 0)
            {
                if (strokeColorEnd != 0)
                {
                    mPaint.color = bgConfig.strokeColor
                    mPaint.shader = LinearGradient(0f, 0f, width.toFloat(), 0f, bgConfig.strokeColor, bgConfig.strokeColorEnd, Shader.TileMode.CLAMP)
                }
                else
                {
                    mPaint.shader = null
                    mPaint.color = bgConfig.strokeColor
                }
            }
            invalidate()
        }
    override var cornerBotLeft: Int = 0
        set(value)
        {
            field = value
            bgConfig.botLeft = value
            initBackground(bgConfig)
            path.reset()
            refreshCornerList()
            if (strokeWidth > 0)
            {
                if (strokeColorEnd != 0)
                {
                    mPaint.color = bgConfig.strokeColor
                    mPaint.shader = LinearGradient(0f, 0f, width.toFloat(), 0f, bgConfig.strokeColor, bgConfig.strokeColorEnd, Shader.TileMode.CLAMP)
                }
                else
                {
                    mPaint.shader = null
                    mPaint.color = bgConfig.strokeColor
                }
            }
            invalidate()
        }

    override var cornerBotRight: Int = 0
        set(value)
        {
            field = value
            bgConfig.botRight = value
            initBackground(bgConfig)
            path.reset()
            refreshCornerList()
            if (strokeWidth > 0)
            {
                if (strokeColorEnd != 0)
                {
                    mPaint.color = bgConfig.strokeColor
                    mPaint.shader = LinearGradient(0f, 0f, width.toFloat(), 0f, bgConfig.strokeColor, bgConfig.strokeColorEnd, Shader.TileMode.CLAMP)
                }
                else
                {
                    mPaint.shader = null
                    mPaint.color = bgConfig.strokeColor
                }
            }
            invalidate()
        }

    override var strokeWidth: Int = 0
    override var strokeColor: Int = 0

    override var isThousandsSeparate: Boolean = false
        set(value)
        {
            field = value
            text = text
        }
    override var minNumberInLeftDecimal: Int = -1
        set(value)
        {
            field = value
            if (minNumberInLeftDecimal > 0)
            {
                textMinNumberInLeftSeparate(text).let()
                {
                    log(" number ", " $it")
                    text = it
                }
            }
        }
    override var roundWithNumberInRightDecimal: Int = -1
    override var cutWithNumberInRightDecimal: Int = -1

    private var m_isSetDateObject = false

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    {
        initDeclareStyleable(attrs)
        bgConfig.bgColor = bgColor
        bgConfig.bgColorEnd = bgColorEnd
        bgConfig.topLeft = cornerTopLeft
        bgConfig.topRight = cornerTopRight
        bgConfig.botLeft = cornerBotLeft
        bgConfig.botRight = cornerBotRight
        bgConfig.strokeColor = strokeColor
        bgConfig.strokeColorEnd = strokeColorEnd
        bgConfig.strokeWidth = strokeWidth
        initBackground(bgConfig)
        refreshCornerList()
    }

    private fun refreshCornerList()
    {
        mCornerRadius = FloatArray(8)
        {
            when (it)
            {
                0, 1 -> bgConfig.topLeft.toFloat()
                2, 3 -> bgConfig.topRight.toFloat()
                4, 5 -> bgConfig.botRight.toFloat()
                6, 7 -> bgConfig.botLeft.toFloat()
                else -> 0f
            }
        }
    }

    private fun initDeclareStyleable(attrs: AttributeSet?)
    {
        val typeArray = context!!.obtainStyledAttributes(attrs, R.styleable.TLTextView)

        bgColor = typeArray.getColor(R.styleable.TLTextView_backgroundColor, Color.TRANSPARENT)
        bgColorEnd = typeArray.getColor(R.styleable.TLTextView_backgroundColorEnd, Color.TRANSPARENT)

        cornerTopLeft = typeArray.getDimensionPixelOffset(R.styleable.TLTextView_corner_top_left, 0)
        cornerTopRight = typeArray.getDimensionPixelOffset(R.styleable.TLTextView_corner_top_right, 0)
        cornerBotLeft = typeArray.getDimensionPixelOffset(R.styleable.TLTextView_corner_bot_left, 0)
        cornerBotRight = typeArray.getDimensionPixelOffset(R.styleable.TLTextView_corner_bot_right, 0)

        strokeWidth = typeArray.getDimensionPixelOffset(R.styleable.TLTextView_strokeWidth, 0)
        strokeColor = typeArray.getColor(R.styleable.TLTextView_strokeColor, 0)
        strokeColorEnd = typeArray.getColor(R.styleable.TLTextView_strokeColorEnd, 0)

        isThousandsSeparate = typeArray.getBoolean(R.styleable.TLTextView_isThousandsSeparate, false)
        minNumberInLeftDecimal = typeArray.getInt(R.styleable.TLTextView_minNumberInLeftDecimal, -1)
        roundWithNumberInRightDecimal = typeArray.getInt(R.styleable.TLTextView_roundWithNumberInRightDecimal, -1)
        cutWithNumberInRightDecimal = typeArray.getInt(R.styleable.TLTextView_cutWithNumberInRightDecimal, -1)

        dateOutputFormat = typeArray.getString(R.styleable.TLTextView_dateOutputFormat)
        dateInputFormat = typeArray.getString(R.styleable.TLTextView_dateInputFormat)

        typeArray.recycle()
    }

    override fun setText(text: CharSequence?, type: BufferType?)
    {

        val t = StringBuilder(text ?: "")
        if (isThousandsSeparate)
        {
            textThousandSeparate(t)
        }

        super.setText(t, type)
        if (m_isSetDateObject)
        {
            m_isSetDateObject = false
        }
        else
        {
            if (!dateInputFormat.isNullOrEmpty() && !dateInputFormat.isNullOrBlank())
            {
                try
                {
                    dateObject = SimpleDateFormat(dateInputFormat, Locale.getDefault()).parse(t.toString())
                }
                catch (e: Exception)
                {
                    e.printStackTrace()
                }
            }
        }
    }

    private fun textThousandSeparate(t: StringBuilder)
    {
        log(" text need format ", " $t")
        if (t.isNumber())
        {
            val decimalSeparator = '.'
            //decimalFormatSymbols.decimalSeparator
            val groupingSeparator = ','
            //decimalFormatSymbols.groupingSeparator

            if (roundWithNumberInRightDecimal <= 0)
            {
                if (t.lastIndexOf(groupingSeparator.toString()) != -1)
                {
                    t.replace(t.lastIndexOf(groupingSeparator.toString()), t.count(), "")
                }
                if (t.lastIndexOf(decimalSeparator.toString()) != -1)
                {
                    t.replace(t.lastIndexOf(decimalSeparator.toString()), t.count(), "")
                }
            }
            else
            {

            }

            val indexGroup = t.indexOf(groupingSeparator)
            if (indexGroup >= 0)
            {
                t.replace(indexGroup, indexGroup + 1, decimalSeparator.toString())

            }

            var numberInLeft = if (t.contains(decimalSeparator))
            {
                t.indexOf(decimalSeparator)
            }
            else
            {
                t.length
            }
            if (minNumberInLeftDecimal > -1)
            {
                val count = minNumberInLeftDecimal - numberInLeft
                if (count > 0)
                {
                    for (i in 0 until count)
                    {
                        t.insert(0, "0")
                        numberInLeft++
                    }
                }
            }

            val firstIndex = numberInLeft - 1
            var index = firstIndex - 1

            while (index >= 0)
            {
                if ((firstIndex - index) % 3 == 0 && t[index] != '-' && t[index] != '+')
                {
                    t.insert(index + 1, groupingSeparator)
                }
                log(" text number", " $t")
                index--
            }
        }
    }

    private fun textMinNumberInLeftSeparate(t: CharSequence): CharSequence?
    {
        if (minNumberInLeftDecimal <= -1)
            return null
        val decimalSeparator = '.'
        //decimalFormatSymbols.decimalSeparator
        val groupingSeparator = ','
        //decimalFormatSymbols.groupingSeparator
        val s = StringBuilder(t)
        s.removePrefix(groupingSeparator.toString())

        var index = s.indexOf(groupingSeparator)
        while (index != -1)
        {
            s.deleteCharAt(index)
            index = s.indexOf(groupingSeparator)
        }
        if (s.isNumber())
        {

            val numberInLeft = if (s.contains(decimalSeparator))
            {
                s.indexOf(decimalSeparator)
            }
            else
            {
                s.length
            }
            val count = minNumberInLeftDecimal - numberInLeft
            if (count > 0)
            {
                for (i in 0 until count)
                {
                    s.insert(0, "0")
                }
            }
            return s
        }
        return null
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int)
    {
        super.onSizeChanged(w, h, oldw, oldh)
        path.reset()
        if (strokeWidth > 0)
        {
            if (strokeColorEnd != 0)
            {
                mPaint.shader = LinearGradient(0f, 0f, w.toFloat(), 0f, bgConfig.strokeColor, bgConfig.strokeColorEnd, Shader.TileMode.CLAMP)
            }
            else
            {
                mPaint.color = bgConfig.strokeColor
            }
        }
    }

    override fun draw(canvas: Canvas?)
    {
        if (strokeWidth > 0)
        {
            path.addRoundRect(RectF(0f, 0f, width.toFloat(), height.toFloat()), mCornerRadius, Path.Direction.CW)
            canvas!!.clipPath(path)
            canvas.drawRect(RectF(0f, 0f, width.toFloat(), height.toFloat()), mPaint)
        }
        super.draw(canvas)
    }
}