package com.taplifetechnology.androidlibrary.TLView

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.LinearGradient
import android.graphics.Paint
import android.graphics.Path
import android.graphics.RectF
import android.graphics.Shader
import android.graphics.drawable.GradientDrawable

import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.widget.EditText
import androidx.annotation.IntegerRes
import androidx.appcompat.widget.AppCompatEditText
import com.taplifetechnology.androidlibrary.BackgroundConfig
import com.taplifetechnology.androidlibrary.R
import com.taplifetechnology.androidlibrary.iview.IBackgroundColor
import com.taplifetechnology.androidlibrary.iview.ICornerView
import com.taplifetechnology.androidlibrary.iview.IStrokeView
import com.taplifetechnology.androidlibrary.initBackground
import com.taplifetechnology.androidlibrary.log
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.Locale

open class TLEditText : AppCompatEditText,
    IBackgroundColor,
    ICornerView,
    IStrokeView
{
    override var bgColorEnd: Int = 0
    override var strokeColorEnd: Int = 0
    override var bgColor: Int = 0

    override var cornerTopLeft: Int = 0
    override var cornerTopRight: Int = 0
    override var cornerBotLeft: Int = 0
    override var cornerBotRight: Int = 0

    override var strokeWidth: Int = 0
    override var strokeColor: Int = 0

    //    override var isThousandsSeparate: Boolean = false
    //    override var minNumberInLeftDecimal: Int = 0
    //    override var roundWithNumberInRightDecimal: Int = 0
    //    override var cutWithNumberInRightDecimal: Int = 0

    private var currencyTextWatcher: CurrencyTextWatcher? = null

    private val bgConfig = BackgroundConfig()

    private val path = Path()
    private val mPaint = Paint()
    private lateinit var mCornerRadius: FloatArray

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    {
        initDeclareStyleable(attrs)
        bgConfig.bgColor = bgColor
        bgConfig.bgColorEnd = bgColorEnd
        bgConfig.topLeft = cornerTopLeft
        bgConfig.topRight = cornerTopRight
        bgConfig.botLeft = cornerBotLeft
        bgConfig.botRight = cornerBotRight
        bgConfig.strokeColor = strokeColor
        bgConfig.strokeColorEnd = strokeColorEnd
        bgConfig.strokeWidth = strokeWidth
        initBackground(bgConfig)
        //        initCurrency()
        mCornerRadius = FloatArray(8)
        {
            when (it)
            {
                0, 1 -> bgConfig.topLeft.toFloat()
                2, 3 -> bgConfig.topRight.toFloat()
                4, 5 -> bgConfig.botRight.toFloat()
                6, 7 -> bgConfig.botLeft.toFloat()
                else -> 0f
            }
        }
    }

    protected fun initDeclareStyleable(attrs: AttributeSet?)
    {
        val typeArray = context!!.obtainStyledAttributes(attrs, R.styleable.TLEditText)

        //        isThousandsSeparate = typeArray.getBoolean(R.styleable.TLEditText_isThousandsSeparate, false)
        //        minNumberInLeftDecimal = typeArray.getInt(R.styleable.TLEditText_minNumberInLeftDecimal, 0)
        //        roundWithNumberInRightDecimal = typeArray.getInt(R.styleable.TLEditText_roundWithNumberInRightDecimal, 0)

        bgColor = typeArray.getColor(R.styleable.TLEditText_backgroundColor, Color.TRANSPARENT)
        bgColorEnd = typeArray.getColor(R.styleable.TLEditText_backgroundColorEnd, Color.TRANSPARENT)

        cornerTopLeft = typeArray.getDimensionPixelOffset(R.styleable.TLEditText_corner_top_left, 0)
        cornerTopRight = typeArray.getDimensionPixelOffset(R.styleable.TLEditText_corner_top_right, 0)
        cornerBotLeft = typeArray.getDimensionPixelOffset(R.styleable.TLEditText_corner_bot_left, 0)
        cornerBotRight = typeArray.getDimensionPixelOffset(R.styleable.TLEditText_corner_bot_right, 0)

        strokeWidth = typeArray.getDimensionPixelOffset(R.styleable.TLEditText_strokeWidth, 0)
        strokeColor = typeArray.getColor(R.styleable.TLEditText_strokeColor, 0)
        strokeColorEnd = typeArray.getColor(R.styleable.TLEditText_strokeColorEnd, 0)

        typeArray.recycle()
    }

    protected fun initBackground()
    {
        val shape = GradientDrawable()
        shape.shape = GradientDrawable.RECTANGLE
        shape.cornerRadii = FloatArray(8)
        {
            when (it)
            {
                0, 1 -> cornerTopLeft.toFloat()
                2, 3 -> cornerTopRight.toFloat()
                4, 5 -> cornerBotRight.toFloat()
                6, 7 -> cornerBotLeft.toFloat()
                else -> 0f
            }
        }
        shape.setColor(bgColor)
        shape.setStroke(strokeWidth, strokeColor)
        background = shape
    }

    private fun initCurrency()
    {
        //        if (isThousandsSeparate)
        //        {
        //            val decimalFormatSymbols = DecimalFormatSymbols.getInstance()
        //            val decimalSeparator = decimalFormatSymbols.decimalSeparator
        //            val groupingSeparator = decimalFormatSymbols.groupingSeparator
        //            keyListener = DigitsKeyListener.getInstance("0123456789$decimalSeparator$groupingSeparator")
        //            if (currencyTextWatcher == null)
        //            {
        //                currencyTextWatcher = CurrencyTextWatcher(this)
        //            }
        ////            this.addTextChangedListener(currencyTextWatcher)
        //        } else
        //        {
        //
        //        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int)
    {
        super.onSizeChanged(w, h, oldw, oldh)
        path.reset()
        if (strokeWidth > 0)
        {
            if (strokeColorEnd != 0)
            {
                mPaint.color = bgConfig.strokeColor
                mPaint.shader = LinearGradient(0f, 0f, w.toFloat(), 0f, bgConfig.strokeColor, bgConfig.strokeColorEnd, Shader.TileMode.CLAMP)
            }
            else
            {
                mPaint.shader = null
                mPaint.color = bgConfig.strokeColor
            }
        }
    }

    override fun draw(canvas: Canvas?)
    {
        if (strokeWidth > 0)
        {
            path.addRoundRect(RectF(0f, 0f, width.toFloat(), height.toFloat()), mCornerRadius, Path.Direction.CW)
            canvas!!.clipPath(path)
            canvas.drawRect(RectF(0f, 0f, width.toFloat(), height.toFloat()), mPaint)
        }
        super.draw(canvas)
    }

    fun getRealNumber(): Number
    {
        return try
        {
            val t = text.toString()
            var number = t.replace("[,]".toRegex(), "")
            BigDecimal(number)
        }
        catch (e: Exception)
        {
            0
        }
    }

    fun setBorderStrokeColor(color: Int, @IntegerRes colorEnd: Int? = null)
    {
        strokeColor = color
        strokeColorEnd = colorEnd ?: color
        bgConfig.strokeColor = strokeColor
        bgConfig.strokeColorEnd = strokeColorEnd
        initBackground(bgConfig)
        if (strokeWidth > 0)
        {
            if (strokeColorEnd != 0)
            {
                mPaint.color = bgConfig.strokeColor
                mPaint.shader = LinearGradient(0f, 0f, width.toFloat(), 0f, bgConfig.strokeColor, bgConfig.strokeColorEnd, Shader.TileMode.CLAMP)
            }
            else
            {
                mPaint.shader = null
                mPaint.color = bgConfig.strokeColor
            }
        }
        invalidate()
    }

    private class CurrencyTextWatcher internal constructor(private val editText: EditText) : TextWatcher
    {
        private var previousCleanString: String? = null

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int)
        {
            // do nothing
        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int)
        {
            // do nothing
        }

        override fun afterTextChanged(editable: Editable)
        {
            val str = editable.toString().trim()

            // cleanString this the string which not contain prefix and ,
            val cleanString = str.replace("[,]".toRegex(), "")
            // for prevent afterTextChanged recursive call
            if (cleanString == previousCleanString || cleanString.isEmpty())
            {
                return
            }
            previousCleanString = cleanString

            val formattedString: String
            formattedString = if (cleanString.contains("."))
            {
                formatDecimal(cleanString)
            }
            else
            {
                formatInteger(cleanString)
            }
            editText.removeTextChangedListener(this) // Remove listener
            editText.setText(formattedString)
            handleSelection()
            editText.addTextChangedListener(this) // Add back the listener
        }

        private fun formatInteger(str: String): String
        {
            val parsed = BigDecimal(str)
            val formatter: DecimalFormat = DecimalFormat("#,###", DecimalFormatSymbols(Locale.US))

            if (parsed == BigDecimal.ZERO)
            {
                return formatter.format(0)
            }
            else
            {
                return formatter.format(parsed)
            }
        }

        private fun formatDecimal(str: String): String
        {
            val parsed = BigDecimal(str.trim())
            // example pattern VND #,###.00
            log("parsed:", "$parsed")
            val decimal = (parsed.toDouble() * 100).toInt() % 100

            if (decimal == 0)
            {
                if (str.contains('.'))
                {
                    val formatter = DecimalFormat("#,###.",
                        DecimalFormatSymbols(Locale.US))
                    formatter.roundingMode = RoundingMode.DOWN
                    return formatter.format(parsed.toInt())
                }
                else
                {
                    val formatter = DecimalFormat("#,###",
                        DecimalFormatSymbols(Locale.US))
                    formatter.roundingMode = RoundingMode.DOWN
                    return formatter.format(parsed.toInt())
                }

            }
            else
            {
                val formatter = DecimalFormat("#,###." + getDecimalPattern(str),
                    DecimalFormatSymbols(Locale.US))
                formatter.roundingMode = RoundingMode.DOWN
                log("return: ", formatter.format(parsed))
                return formatter.format(parsed)
            }
        }

        /**
         * It will return suitable pattern for format decimal
         * For example: 10.2 -> return 0 | 10.23 -> return 00, | 10.235 -> return 000
         */
        private fun getDecimalPattern(str: String): String
        {
            var decimalCount = str.length - str.indexOf(".") - 1
            if (decimalCount > 2)
            {
                decimalCount = 2
            }
            val decimalPattern = StringBuilder()
            var i = 0
            while (i < decimalCount)
            {
                decimalPattern.append("0")
                i++
            }
            return decimalPattern.toString()
        }

        private fun handleSelection()
        {
            editText.setSelection(editText.text.length)
            //            if (editText.text.length <= MAX_LENGTH)
            //            {
            //            }
            //            else
            //            {
            //                editText.setSelection(MAX_LENGTH)
            //            }
        }
    }

}