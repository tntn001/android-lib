package com.taplifetechnology.androidlibrary.TLView

import android.content.Context
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView
import com.taplifetechnology.androidlibrary.R
import com.taplifetechnology.androidlibrary.iview.ICornerView
import com.taplifetechnology.androidlibrary.iview.IStrokeView
import com.taplifetechnology.androidlibrary.log

class TLImageView: AppCompatImageView, IStrokeView, ICornerView
{
    override var strokeWidth: Int = 0
    override var strokeColor: Int = 0
    override var strokeColorEnd: Int = 0

    override var cornerTopLeft: Int = 0
    override var cornerTopRight: Int = 0
    override var cornerBotLeft: Int = 0
    override var cornerBotRight: Int = 0

    private val mWidth: Int = 0
    private val mHeight: Int = 0

    private var mDrawable: Drawable? = null
    private var mIsRound: Boolean = false
    private lateinit var mCornerRadius: FloatArray
    private val path = Path()

    constructor(context: Context?): super(context)
    constructor(context: Context?, attrs: AttributeSet?): super(context, attrs)
    {
        initDeclareStylable(attrs)
        initDrawable()
        mCornerRadius = FloatArray(8)
        {
            when(it)
            {
                0, 1 -> cornerTopLeft.toFloat()
                2, 3 -> cornerTopRight.toFloat()
                4, 5 -> cornerBotRight.toFloat()
                6, 7 -> cornerBotLeft.toFloat()
                else -> 0f
            }
        }
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int): super(context,
            attrs,
            defStyleAttr)

    private fun initDeclareStylable(attrs: AttributeSet?)
    {
        val typeArray = context!!.obtainStyledAttributes(attrs, R.styleable.TLImageView)
        mDrawable = typeArray.getDrawable(R.styleable.TLImageView_drawable)
        mIsRound = typeArray.getBoolean(R.styleable.TLImageView_isRound, false)
        strokeWidth = typeArray.getDimensionPixelOffset(R.styleable.TLImageView_strokeWidth, 0)
        strokeColor = typeArray.getColor(R.styleable.TLImageView_strokeColor, 0)
        strokeColorEnd = typeArray.getColor(R.styleable.TLImageView_strokeColorEnd, 0)

        cornerBotLeft =
                typeArray.getDimensionPixelOffset(R.styleable.TLImageView_corner_bot_left, 0)
        cornerBotRight =
                typeArray.getDimensionPixelOffset(R.styleable.TLImageView_corner_bot_right, 0)
        cornerTopLeft =
                typeArray.getDimensionPixelOffset(R.styleable.TLImageView_corner_top_left, 0)
        cornerTopRight =
                typeArray.getDimensionPixelOffset(R.styleable.TLImageView_corner_top_right, 0)


        if(mIsRound)
        {
            cornerBotLeft = 999
            cornerBotRight = 999
            cornerTopRight = 999
            cornerTopLeft = 999
        }

        typeArray.recycle()
    }

    private fun initDrawable()
    {
        val bitmap = drawableToBitmap(mDrawable)
        bitmap?.let()
        {
            if(strokeColorEnd != 0)
            {

            }
            if(mIsRound)
            {
                setImageDrawable(RoundDrawable(it, strokeWidth.toFloat(), strokeColor))
            }
            else
            {
                mCornerRadius = FloatArray(8)
                {
                    when(it)
                    {
                        0, 1 -> cornerTopLeft.toFloat()
                        2, 3 -> cornerTopRight.toFloat()
                        4, 5 -> cornerBotRight.toFloat()
                        6, 7 -> cornerBotLeft.toFloat()
                        else -> 0f
                    }
                }
                setImageDrawable(RectDrawable(it,
                        strokeWidth.toFloat(),
                        strokeColor,
                        measuredWidth,
                        measuredHeight,
                        mCornerRadius))

            }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int)
    {
        super.onSizeChanged(w, h, oldw, oldh)
        path.reset()
    }

    override fun draw(canvas: Canvas?)
    {
        mCornerRadius = FloatArray(8)
        {
            when(it)
            {
                0, 1 -> cornerTopLeft.toFloat()
                2, 3 -> cornerTopRight.toFloat()
                4, 5 -> cornerBotRight.toFloat()
                6, 7 -> cornerBotLeft.toFloat()
                else -> 0f
            }
        }
        path.addRoundRect(RectF(0f, 0f, width.toFloat(), height.toFloat()),
                mCornerRadius,
                Path.Direction.CW)
        canvas!!.clipPath(path)
        super.draw(canvas)
    }

    fun setTLDrawable(drawable: Drawable)
    {
        mDrawable = drawable
        initDrawable()
    }

    fun cornerImage(radius: Int)
    {
        cornerBotLeft = radius
        cornerBotRight = radius
        cornerTopRight = radius
        cornerTopLeft = radius
        mCornerRadius = FloatArray(8)
        {
            when(it)
            {
                0, 1 -> cornerTopLeft.toFloat()
                2, 3 -> cornerTopRight.toFloat()
                4, 5 -> cornerBotRight.toFloat()
                6, 7 -> cornerBotLeft.toFloat()
                else -> 0f
            }
        }
        initDrawable()
    }

    fun roundImage()
    {
        mIsRound = true
        cornerBotLeft = 999
        cornerBotRight = 999
        cornerTopRight = 999
        cornerTopLeft = 999
        mCornerRadius = FloatArray(8)
        {
            when(it)
            {
                0, 1 -> cornerTopLeft.toFloat()
                2, 3 -> cornerTopRight.toFloat()
                4, 5 -> cornerBotRight.toFloat()
                6, 7 -> cornerBotLeft.toFloat()
                else -> 0f
            }
        }
        initDrawable()
    }
}

fun drawableToBitmap(drawable: Drawable?): Bitmap?
{
    drawable?.let()
    {
        if(drawable is BitmapDrawable)
        {
            return drawable.bitmap
        }

        var width = drawable.intrinsicWidth
        width = if(width > 0) width else 1
        var height = drawable.intrinsicHeight
        height = if(height > 0) height else 1

        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)
        return bitmap
    }
    return null
}


private class RoundDrawable(wrappedBitmap: Bitmap,
                            strokeWidth: Float,
                            strokeColor: Int,
                            val roundRectRadius: Int = 0): Drawable()
{
    var bitmap: Bitmap = wrappedBitmap
    var maskPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    var borderPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    var side: Int = 0
    var radius: Float = 0.toFloat()

    init
    {
        if(strokeWidth > 0)
        {
            borderPaint.style = Paint.Style.STROKE
            borderPaint.strokeWidth = strokeWidth
            borderPaint.color = strokeColor
        }
        else
        {
            borderPaint.style = Paint.Style.FILL
            borderPaint.color = Color.TRANSPARENT
        }
        side = Math.min(bitmap.width, bitmap.height)
    }

    override fun onBoundsChange(bounds: Rect)
    {
        val matrix = Matrix()
        val src = RectF(0f, 0f, side.toFloat(), side.toFloat())
        src.offset((bitmap.width - side) / 2f, (bitmap.height - side) / 2f)
        val dst = RectF(bounds)
        if(borderPaint.strokeWidth > 0)
        {
            dst.inset(borderPaint.strokeWidth, borderPaint.strokeWidth)
        }
        matrix.setRectToRect(src, dst, Matrix.ScaleToFit.CENTER)

        val shader = BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)
        shader.setLocalMatrix(matrix)
        maskPaint.shader = shader
        matrix.mapRect(src)
        radius = src.width() / 2f
    }

    override fun draw(canvas: Canvas)
    {
        val b = bounds
        if(roundRectRadius == 0)
        {
            canvas.drawCircle(b.exactCenterX(), b.exactCenterY(), radius, maskPaint)
            canvas.drawCircle(b.exactCenterX(),
                    b.exactCenterY(),
                    radius + borderPaint.strokeWidth / 2,
                    borderPaint)
        }
        else
        {
            log(" TL image view ", " draw round rect")
            canvas.drawRoundRect(b.left.toFloat(),
                    b.top.toFloat(),
                    b.right.toFloat(),
                    b.bottom.toFloat(),
                    roundRectRadius.toFloat(),
                    roundRectRadius.toFloat(),
                    borderPaint)
        }
    }

    override fun setAlpha(alpha: Int)
    {
    }

    override fun setColorFilter(cf: ColorFilter?)
    {
    }

    override fun getOpacity(): Int
    {
        return PixelFormat.TRANSLUCENT
    }
}

private class RectDrawable(wrappedBitmap: Bitmap,
                           val strokeWidth: Float,
                           strokeColor: Int,
                           val width: Int,
                           val height: Int,
                           val cornerRadius: FloatArray = FloatArray(8) { 0f }): Drawable()
{
    var bitmap: Bitmap = wrappedBitmap
    var maskPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    var borderPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    var side: Int = 0

    init
    {
        if(strokeWidth > 0)
        {
            borderPaint.style = Paint.Style.STROKE
            borderPaint.strokeWidth = strokeWidth
            borderPaint.color = strokeColor
        }
        else
        {
            borderPaint.style = Paint.Style.FILL
            borderPaint.color = Color.TRANSPARENT
        }
        side = Math.min(bitmap.width, bitmap.height)
    }

    override fun onBoundsChange(bounds: Rect)
    {
        val matrix = Matrix()
        val src = RectF(0f, 0f, side.toFloat(), side.toFloat())
        src.offset((bitmap.width - side) / 2f, (bitmap.height - side) / 2f)
        val dst = RectF(bounds)
        if(borderPaint.strokeWidth > 0)
        {
            dst.inset(borderPaint.strokeWidth, borderPaint.strokeWidth)
        }
        matrix.setRectToRect(src, dst, Matrix.ScaleToFit.CENTER)

        val shader = BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)
        shader.setLocalMatrix(matrix)
        maskPaint.shader = shader
        matrix.mapRect(src)
    }

    override fun draw(canvas: Canvas)
    {
        canvas.drawRect(RectF(0f, 0f, width.toFloat(), height.toFloat()), maskPaint)
        if(strokeWidth > 0)
        {
            canvas.drawRect(
                    RectF(0f + strokeWidth / 2, 0f + strokeWidth / 2,
                            width.toFloat() - strokeWidth / 2,
                            height.toFloat() - strokeWidth / 2), borderPaint)
        }


    }

    override fun setAlpha(alpha: Int)
    {
    }

    override fun setColorFilter(cf: ColorFilter?)
    {
    }

    override fun getOpacity(): Int
    {
        return PixelFormat.TRANSLUCENT
    }
}