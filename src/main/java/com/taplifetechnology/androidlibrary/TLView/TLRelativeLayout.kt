package com.taplifetechnology.androidlibrary.TLView

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.LinearGradient
import android.graphics.Paint
import android.graphics.Path
import android.graphics.RectF
import android.graphics.Shader
import android.util.AttributeSet
import android.widget.RelativeLayout
import com.taplifetechnology.androidlibrary.BackgroundConfig
import com.taplifetechnology.androidlibrary.R
import com.taplifetechnology.androidlibrary.iview.IBackgroundColor
import com.taplifetechnology.androidlibrary.iview.ICornerView
import com.taplifetechnology.androidlibrary.iview.IStrokeView
import com.taplifetechnology.androidlibrary.initBackground

open class TLRelativeLayout : RelativeLayout,
    IBackgroundColor,
    ICornerView,
    IStrokeView
{
    override var bgColorEnd: Int = 0
    override var strokeColorEnd: Int = 0
    override var bgColor: Int = 0
    override var cornerTopLeft: Int = 0
    override var cornerTopRight: Int = 0
    override var cornerBotLeft: Int = 0
    override var cornerBotRight: Int = 0
    override var strokeWidth: Int = 0
    override var strokeColor: Int = 0

    private val bgConfig = BackgroundConfig()
    private val path = Path()
    private val mPaint = Paint()
    private lateinit var mCornerRadius: FloatArray

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    {
        initDeclareStyleable(attrs)
        bgConfig.bgColor = bgColor
        bgConfig.bgColorEnd = bgColorEnd
        bgConfig.topLeft = cornerTopLeft
        bgConfig.topRight = cornerTopRight
        bgConfig.botLeft = cornerBotLeft
        bgConfig.botRight = cornerBotRight
        bgConfig.strokeColor = strokeColor
        bgConfig.strokeColorEnd = strokeColorEnd
        bgConfig.strokeWidth = strokeWidth
        initBackground(bgConfig)
        mCornerRadius = FloatArray(8)
        {
            when (it)
            {
                0, 1 -> bgConfig.topLeft.toFloat()
                2, 3 -> bgConfig.topRight.toFloat()
                4, 5 -> bgConfig.botRight.toFloat()
                6, 7 -> bgConfig.botLeft.toFloat()
                else -> 0f
            }
        }
    }

    private fun initDeclareStyleable(attrs: AttributeSet?)
    {
        val typeArray = context!!.obtainStyledAttributes(attrs, R.styleable.TLRelativeLayout)
        bgColor = typeArray.getColor(R.styleable.TLRelativeLayout_backgroundColor, Color.TRANSPARENT)
        bgColorEnd = typeArray.getColor(R.styleable.TLRelativeLayout_backgroundColorEnd, Color.TRANSPARENT)

        cornerTopLeft = typeArray.getDimensionPixelOffset(R.styleable.TLRelativeLayout_corner_top_left, 0)
        cornerTopRight = typeArray.getDimensionPixelOffset(R.styleable.TLRelativeLayout_corner_top_right, 0)
        cornerBotLeft = typeArray.getDimensionPixelOffset(R.styleable.TLRelativeLayout_corner_bot_left, 0)
        cornerBotRight = typeArray.getDimensionPixelOffset(R.styleable.TLRelativeLayout_corner_bot_right, 0)

        strokeWidth = typeArray.getDimensionPixelOffset(R.styleable.TLRelativeLayout_strokeWidth, 0)
        strokeColor = typeArray.getColor(R.styleable.TLRelativeLayout_strokeColor, 0)
        strokeColorEnd = typeArray.getColor(R.styleable.TLRelativeLayout_strokeColorEnd, 0)

        typeArray.recycle()
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int)
    {
        super.onSizeChanged(w, h, oldw, oldh)
        if (strokeWidth > 0)
        {
            path.reset()
            if (strokeColorEnd != 0)
            {
                mPaint.shader = LinearGradient(0f, 0f, w.toFloat(), 0f, bgConfig.strokeColor, bgConfig.strokeColorEnd, Shader.TileMode.CLAMP)
            }
            else
            {
                mPaint.color = bgConfig.strokeColor
            }
        }
    }

    override fun draw(canvas: Canvas?)
    {
        if (strokeWidth > 0)
        {
            path.addRoundRect(RectF(0f, 0f, width.toFloat(), height.toFloat()), mCornerRadius, Path.Direction.CW)
            canvas!!.clipPath(path)
            canvas.drawRect(RectF(0f, 0f, width.toFloat(), height.toFloat()), mPaint)
        }
        super.draw(canvas)
    }
}
