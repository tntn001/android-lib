package com.taplifetechnology.androidlibrary.TLView

import android.content.Context
import android.graphics.Color

import android.util.AttributeSet
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.taplifetechnology.androidlibrary.BackgroundConfig
import com.taplifetechnology.androidlibrary.R
import com.taplifetechnology.androidlibrary.iview.IBackgroundColor
import com.taplifetechnology.androidlibrary.iview.ICornerView
import com.taplifetechnology.androidlibrary.iview.IStrokeView
import com.taplifetechnology.androidlibrary.initBackground

class TLFloatingButton : FloatingActionButton, IBackgroundColor, ICornerView, IStrokeView
{
    override var bgColor: Int = 0
    override var bgColorEnd: Int = 0

    override var cornerTopLeft: Int = 0
    override var cornerTopRight: Int = 0
    override var cornerBotLeft: Int = 0
    override var cornerBotRight: Int = 0
    override var strokeWidth: Int = 0
    override var strokeColor: Int = 0
    override var strokeColorEnd: Int = 0

    private val bgConfig = BackgroundConfig()
    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    {
        initDeclareStyleable(attrs)
        bgConfig.bgColor = bgColor
        bgConfig.bgColorEnd = bgColorEnd
        bgConfig.topLeft = cornerTopLeft
        bgConfig.topRight = cornerTopRight
        bgConfig.botLeft = cornerBotLeft
        bgConfig.botRight = cornerBotRight
        bgConfig.strokeColor = strokeColor
        bgConfig.strokeColorEnd = strokeColorEnd
        bgConfig.strokeWidth = strokeWidth
        if (bgColor != 0)
        {
            initBackground(bgConfig)
        }
    }
    private fun initDeclareStyleable(attrs: AttributeSet?)
    {
        val typeArray = context!!.obtainStyledAttributes(attrs, R.styleable.TLFloatingButton)
        bgColor = typeArray.getColor(R.styleable.TLFloatingButton_backgroundColor, Color.TRANSPARENT)
        bgColorEnd = typeArray.getColor(R.styleable.TLFloatingButton_backgroundColorEnd, Color.TRANSPARENT)

        cornerTopLeft = typeArray.getDimensionPixelOffset(R.styleable.TLFloatingButton_corner_top_left, 0)
        cornerTopRight = typeArray.getDimensionPixelOffset(R.styleable.TLFloatingButton_corner_top_right, 0)
        cornerBotLeft = typeArray.getDimensionPixelOffset(R.styleable.TLFloatingButton_corner_bot_left, 0)
        cornerBotRight = typeArray.getDimensionPixelOffset(R.styleable.TLFloatingButton_corner_bot_right, 0)

        strokeWidth = typeArray.getDimensionPixelOffset(R.styleable.TLFloatingButton_strokeWidth, 0)
        strokeColor = typeArray.getColor(R.styleable.TLFloatingButton_strokeColor, 0)
        strokeColorEnd = typeArray.getColor(R.styleable.TLFloatingButton_strokeColorEnd, 0)

        typeArray.recycle()
    }

}